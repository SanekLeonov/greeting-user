﻿using System;
using System.Text.RegularExpressions;

namespace GreetingUser
{
    class Program
    {
        static string Name;
        static DateTime BirthDate;
        static int Years;

        static void Main(string[] args)
        {
            GreetUser();
            StartDialogWithUser();
            ShowMessageAfterDataEntry();
        }

        static void GreetUser()
        {
            Console.WriteLine("Привет!");
        }

        static void StartDialogWithUser()
        {
            AskName();
            AskBirthDate();
        }

        static void AskName()
        {
            string name;

            Console.WriteLine("Введите имя: ");
            name = Console.ReadLine();

            if (IsCorrectedName(name))
            {
                Name = name;
            }
            else
            {
                ShowErrorAndStartDialogAgain("Вы ввели неккоректное имя. Имя может состоять из латинских или русских букв");
            }
        }

        static void AskBirthDate()
        {
            string birthDay, birthMonth, birthYear;

            Console.WriteLine("Введите день рождения: ");
            birthDay = Console.ReadLine();

            Console.WriteLine("Введите месяц рождения: ");
            birthMonth = Console.ReadLine();

            Console.WriteLine("Введите год рождения: ");
            birthYear = Console.ReadLine();

            if (!IsCorrectDateFormat(birthDay, birthMonth, birthYear))
            {
                ShowErrorAndStartDialogAgain("Неправильный формат ввода даты, дата может состоять только из чисел");
            }

            if (IsCorrectedBirthDate(birthDay, birthMonth, birthYear))
            {
                Years = CalculateYears(DateTime.Now, BirthDate);
            }
            else
            {
                ShowErrorAndStartDialogAgain("Вы ввели некорректную или несуществующую дату");
            }
        }

        static void AskBirthDate(string birthDay, string birthMonth, string birthYear)
        {
            try
            {
                BirthDate = new DateTime(Convert.ToInt32(birthYear),
                    Convert.ToInt32(birthMonth),
                    Convert.ToInt32(birthDay));
            }
            catch (ArgumentOutOfRangeException)
            {
                ShowErrorAndStartDialogAgain("Вы ввели некорректную или несуществующую дату");
            }
        }

        static bool IsCorrectDateFormat(string birthDay, string birthMonth, string birthYear)
        {
            var dateRegexFormat = @"^[0-9]+$";
            var regex = new Regex(dateRegexFormat);

            return regex.IsMatch(birthDay) & regex.IsMatch(birthMonth) & regex.IsMatch(birthYear);
        }

        static void ShowMessageAfterDataEntry()
        {
            var message = $"Привет, {Name}, Ваш возраст равен {Years} лет. Приятно познакомиться.";

            Console.WriteLine(message);
        }


        static int CalculateYears(DateTime firstDateTime, DateTime secondDateTime)
        {
            const double DaysInYear = 365.25;
            var timeSpan = firstDateTime - secondDateTime;
            var years = (int)Math.Floor(timeSpan.TotalDays / DaysInYear);

            return years;
        }

        static bool IsCorrectedBirthDate(string birthDay, string birthMonth, string birthYear)
        {
            AskBirthDate(birthDay, birthMonth, birthYear);

            var yearsBetweenDates = CalculateYears(DateTime.Now, BirthDate);
            var isBirthDateLessCurrentDate = (BirthDate < DateTime.Now) & (yearsBetweenDates > 0);

            return isBirthDateLessCurrentDate;
        }

        static bool IsCorrectedName(string name)
        {
            var nameRegexFormat = @"^[a-zA-Zа-яА-ЯёЁ]+$";
            var regex = new Regex(nameRegexFormat);

            return regex.IsMatch(name);
        }

        static void ShowErrorAndStartDialogAgain(string errorMessage)
        {
            Console.WriteLine(errorMessage);
            StartDialogWithUser();
        }
    }
}
